# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit cmake-utils git-r3

DESCRIPTION="Tool to poke around in a Qt-application and also to manipulate the application to some extent"
HOMEPAGE="http://www.kdab.com/gammaray"
EGIT_REPO_URI="git://github.com/KDAB/GammaRay.git"

DEPEND="
dev-qt/qtcore
dev-qt/qtgui
"

RDEPEND="${DEPEND}"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS=""
IUSE=""
