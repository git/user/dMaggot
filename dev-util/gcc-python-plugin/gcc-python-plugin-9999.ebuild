

	# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PYTHON_COMPAT=( python{2_7,3_4,3_5} )

inherit git-r3 python-r1

DESCRIPTION="GCC Python plugin for static analysis"
HOMEPAGE="https://github.com/davidmalcolm/gcc-python-plugin"
EGIT_REPO_URI="https://github.com/davidmalcolm/gcc-python-plugin.git"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS=""
IUSE="doc"

DEPEND="${DEPEND}
>=sys-devel/gcc-7
dev-python/six
dev-python/pygments
media-gfx/graphviz"

RDEPEND="${DEPEND}"

DEPEND="${DEPEND}
dev-python/sphinx
doc? (
dev-python/pycairo
)"

src_prepare() {
	default
	python_setup
	python_copy_sources
}

src_configure() {
	return
}

src_compile() {
	compile_plugin() {
		PLUGIN_NAME="${EPYTHON}"
		GCC_WITH_PYTHON_SCRIPT_NAME="gcc-with-${PLUGIN_NAME}"

		make plugin || die "Error building plugin"
		cp python.so "${PLUGIN_NAME}.so"
		cp gcc-with-python "${GCC_WITH_PYTHON_SCRIPT_NAME}"
		sed -i -e"s|-fplugin=[^ ]*|-fplugin=${PLUGIN_NAME}|" "${GCC_WITH_PYTHON_SCRIPT_NAME}"
		sed -i -e"s|-fplugin-arg-python-script|-fplugin-arg-${PLUGIN_NAME}-script|" "${GCC_WITH_PYTHON_SCRIPT_NAME}"
	}

	python_foreach_impl run_in_build_dir compile_plugin

	cd "${WORKDIR}/${P}-${EPYTHON}"

	emake man

	if use doc; then
		emake html
	fi
}

src_test() {
	emake testcpybuilder test-suite testcpychecker
}

src_install() {
	GCC_PLUGIN_DIR="$(gcc --print-file-name=plugin)"

	install_plugin_files() {
		dobin "gcc-with-${EPYTHON}"
		insinto "${GCC_PLUGIN_DIR}"
		doins "${EPYTHON}.so"
		insinto "$(python_get_sitedir)"
		doins -r gccutils
		doins -r libcpychecker
	}

	python_foreach_impl run_in_build_dir install_plugin_files
	dobin "${FILESDIR}/gcc-with-python"

	cd "${WORKDIR}/${P}-${EPYTHON}"

	doman docs/_build/man/*

	if use doc; then
		dodoc -r examples
		dodoc -r docs/_build/html
	fi
}
