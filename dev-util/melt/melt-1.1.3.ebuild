# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit eutils versionator

MY_P="$(replace_version_separator '_' '-' ${P})"
DESCRIPTION="High-level domain specific language for extending or customizing the Gnu Compiler Collection"
HOMEPAGE="http://gcc-melt.org/"
SRC_URI="http://gcc-melt.org/melt-plugin-1.1.3-for-gcc-4.8-or-4.9.tar.bz2"
LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="doc"

DEPEND="
doc? ( app-text/texi2html )
>=sys-devel/gcc-4.8
dev-util/unifdef
"

RDEPEND="${DEPEND}"

S="${WORKDIR}/melt-plugin-1.1.3-for-gcc-4.8-or-4.9"

src_prepare() {
	epatch "${FILESDIR}/${PN}-makefile-nodocs.diff"
}

src_configure() { :; }

src_compile() {
	# Build cannot be parallel
	emake -j1

	if use doc; then
		emake melt-gendoc
		emake doc
	fi
}

src_install () {
	emake DESTDIR="${D}" install

	if use doc; then
		dohtml *.html
		dodoc *.pdf
	fi
}
